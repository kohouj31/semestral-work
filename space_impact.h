//Jan Kohout 2020

typedef struct 
{
  bool active[30];
  int pos_x[30];
  int pos_y[30];
  
}enemies_t;

int space_impact(int dif, bool pc);
bool draw_ship(int x, int y, int size);
bool draw_enemy(int x, int y, int num, enemies_t *enemies, bool shot);
void ship_shoot(int x, int y, bool on);
//todo
void enemy(void);
bool hit_compute(int x, int y, int diff, enemies_t *enemies, bool shot, int *score, int *game_end);
void check_hit(int x, int y, int diff, enemies_t *enemies, int *game_end);

//todo
void ship_enemy(int x, int y, int type);
void show_score(int *score);
