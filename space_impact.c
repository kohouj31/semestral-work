//Jan Kohout 2020

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <termios.h>

#include <pthread.h>

//for mem aloc and assert check with better debug
#include "utils.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "snake_impact.h"
#include "space_impact.h"

int space_impact(int dif, bool pc)
{
  bool quit = false;
  char in;
  int score = 0;
  int x = 20;
  int y = 100;
  bool redraw = false;
  bool shot = false;
  int game_end = false;
  enemies_t enemies;
  int start = 1;
  for (int i = 0; i < 30; ++i)
  {
    int num_y = (rand() % (310 - 15 + 1)) + 10;
    int num_x = (rand() % (465 - 215 + 1)) + 215;
    enemies.pos_y[i] = num_y;
    enemies.pos_x[i] = num_x;
    enemies.active[i] = true;
  }


  struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 35 * 1000 * 1000};
  while (quit == false)
  {
    if (game_end == 1)
    {
      draw_string(0, 100, "YOU WIN", 5, 'M', get_color(255, 0, 0));
      sleep(6);
      break;
    }
    if (game_end == 2)
    {
      for (int i = 0; i < 6; ++i)
      {
        draw_string(0, 100, "GAME OVER", 5, 'M', get_color(255, 0, 0));
        sleep(1);
      }
      break;
    }
    if (start == 1)
    {
      display_clean(parlcd_mem_base);
      draw_string(0, 20, "PRESS '2' TO START", 2, 'M', get_color(0, 0, 255));
      draw_string(0, 80, "USE '6', '4', '2', '8' TO MOVE", 2, 'M', get_color(0, 0, 255));
      draw_string(0, 140, "USE '5' TO SHOOT", 2, 'M', get_color(0, 0, 255));
      draw_string(0, 200, "PRESS 'Q' TO EXIT", 2, 'M', get_color(0, 0, 255));
      draw_string(0, 270, "! HAVE FUN !", 2, 'M', get_color(255, 0, 255));
      start = 0;
    }
    if (pc)
    {
      redraw = true;
      int rnd = (rand() % 9);
      if (rnd < 3 && y < 270) {y += 10;}
      else if (rnd > 5 && y > 0) {y -= 10;}
      else if (rnd == 4 && x < 200) {x += 10;}
      else {
        shot = true;
        if (x > 0) {x -= 10;}
      }
      for (int i = 0; i < dif; ++i)
      {
        if (enemies.pos_y[i] >= (y + 5) && (y + 45) >= enemies.pos_y[i]
            && enemies.pos_x[i] >= (x + 45) && (x + 225) >= enemies.pos_x[i])
        {shot = true;}
      }

    }
    else
    {
      if (scanf("%c", &in) == 1)
      {
        switch (in)
        {
        case '2':
          if (y < 270)
          {
            redraw = true;
            *(volatile uint32_t*)(led_mem_base + SPILED_REG_LED_LINE_o) = 0x0;
            y += 10;
          } else {
            fprintf(stderr, "Berrier hit\n");
            *(volatile uint32_t*)(led_mem_base + SPILED_REG_LED_LINE_o) = 0x7fffffff;
          }
          break;
        case '4':
          if (x > 0)
          {
            redraw = true;
            x -= 10;
            *(volatile uint32_t*)(led_mem_base + SPILED_REG_LED_LINE_o) = 0x0;
          } else {
            fprintf(stderr, "Berrier hit\n");
            *(volatile uint32_t*)(led_mem_base + SPILED_REG_LED_LINE_o) = 0x7fffffff;
          }
          break;
        case '5':
          shot = true;
          break;
        case '6':
          if (x < 200)
          {
            redraw = true;
            *(volatile uint32_t*)(led_mem_base + SPILED_REG_LED_LINE_o) = 0x0;
            x += 10;
          } else {
            fprintf(stderr, "Berrier hit\n");
            *(volatile uint32_t*)(led_mem_base + SPILED_REG_LED_LINE_o) = 0x7fffffff;
          }
          break;
        case '8':
          if (y > 0)
          {
            redraw = true;
            *(volatile uint32_t*)(led_mem_base + SPILED_REG_LED_LINE_o) = 0x0;
            y -= 10;
          } else {
            fprintf(stderr, "Berrier hit\n");
            *(volatile uint32_t*)(led_mem_base + SPILED_REG_LED_LINE_o) = 0x7fffffff;
          }
          break;
        case 'q':
          quit = true;
        default:
          redraw = false;
          break;
        }
      }
    }
    if (redraw)
    {
      clean_frame();
      draw_enemy(x, y, dif, &enemies, shot);
      draw_ship(x, y , 20);
      ship_shoot((x + 20), (y + 23), shot);
      display_update(parlcd_mem_base);
      clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
      if (shot == true) {
        hit_compute(x, y, dif, &enemies, shot, &score, &game_end);
        draw_enemy(x, y, dif, &enemies, shot);
        shot = false;
        ship_shoot((x + 20), (y + 23), shot);
        display_update(parlcd_mem_base);
      }
      check_hit( x, y, dif, &enemies, &game_end);
      show_score(&score);

      redraw = true;
    }
  }
  return score;
}
// - function -----------------------------------------------------------------
void show_score(int *score)
{
  *(volatile uint32_t*)(led_mem_base + SPILED_REG_LED_LINE_o) = *score;
}
// - function -----------------------------------------------------------------
bool hit_compute(int x, int y, int diff, enemies_t *enemies, bool shot, int *score, int *game_end)
{
  int count = 0;
  for (int i = 0; i < diff; ++i)
  {
    if (enemies->active[i] == true)
    {
      if (enemies->pos_y[i] >= (y + 5) && (y + 45) >= enemies->pos_y[i]
          && enemies->pos_x[i] >= (x + 45) && (x + 225) >= enemies->pos_x[i]
          && shot == true)
      {
        blink();
        if (i < 7)
        {
          *score += 5;
        } else
        {
          *score += 10;
        }
        enemies->active[i] = false;
        count += 1;
      }
    } else
    {
      count += 1;
    }
  }
  if (count == diff)
  {*game_end = 1;}
  return true;
}
// - function -----------------------------------------------------------------
void check_hit(int x, int y, int diff, enemies_t *enemies, int *game_end)
{
  for (int i = 0; i < diff; ++i)
  {
    if (enemies->active[i] == true)
    {
      if (enemies->pos_y[i] >= (y - 15) && (y + 65) >= enemies->pos_y[i]
          && enemies->pos_x[i] >= (x - 15) && (x + 45) >= enemies->pos_x[i])
      {
        *game_end = 2;
      }
      if (enemies->pos_x[i] < 1)
      {
        *game_end = 2;
      }
    }
  }
}
// - function -----------------------------------------------------------------
bool draw_enemy(int x, int y, int num, enemies_t *enemies, bool shot)
{
  for (int i = 0; i < num; ++i)
  {
    if (enemies->active[i] == true)
    {
      enemies->pos_x[i] -= 4;
      if (i < 7)
      {
        ship_enemy(enemies->pos_x[i], enemies->pos_y[i], 1);
      }
      else {
        ship_enemy(enemies->pos_x[i], enemies->pos_y[i], 0);
      }
    }
  }
  return true;
}

// - function -----------------------------------------------------------------
void ship_shoot(int x, int y , bool on)
{
  for (int cur_y = y; cur_y < (y + 3); ++cur_y)
  {
    for (int cur_x = x; cur_x < (x + 200); ++cur_x)
    {
      if (on)
      {
        draw_pixel(cur_x, cur_y, get_color(0, 255, 255));
      } else
      {
        draw_pixel(cur_x, cur_y, get_color(0, 0, 0));
      }
    }
  }
}

// - function -----------------------------------------------------------------
bool draw_ship(int x, int y, int size)
{
  for (int cur_y = y; cur_y < (y + 50); ++cur_y)
  {
    if ((cur_y - y) > 20 && (cur_y - y) < 30)
    {
      size = 45;
    } else {size = 20;}
    for (int cur_x = x; cur_x < (x + size); ++cur_x)
    {
      draw_pixel(cur_x, cur_y, get_color(255, 0, 0));
    }
  }
  return true;
}

// - function -----------------------------------------------------------------
void ship_enemy(int x, int y, int type)
{
  if (type == 0)
  {
    for (int cur_y = y; cur_y < (y + 15); ++cur_y)
    {
      for (int cur_x = x; cur_x < (x + 15); ++cur_x)
      {
        draw_pixel(cur_x, cur_y, get_color(0, 255, 0));
      }
    }
  } else
  {
    for (int cur_y = y; cur_y < (y + 15); ++cur_y)
    {
      for (int cur_x = x; cur_x < (x + 15); ++cur_x)
      {
        draw_pixel(cur_x, cur_y, get_color(0, 0, 255));
      }
    }
  }
}



