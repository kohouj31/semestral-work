#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <termios.h>
#include <pthread.h>

#include "utils.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "snake.h"
#include "snake_impact.h"
//#include <curses.h>
//#include <conio.h>

typedef struct {
	int end;
} data_t_t;



struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 300 * 1000 * 1000};

int body[100][2];
int food[100][2];
int first = 0;
int last = 0;
int input;
int direction = 2;
int length = 2;

int food_x = 0;
int food_y = 0;

void *move_thread(void *data);

pthread_mutex_t mymtx;
pthread_cond_t variable;


int snake(int speed , int level) {

	//speed setting
	if (speed == 1) {
		loop_delay.tv_nsec = 500 * 1000 * 1000;
	}
	if (speed == 2) {
		loop_delay.tv_nsec = 400 * 1000 * 1000;
	}
	if (speed == 3) {
		loop_delay.tv_nsec = 300 * 1000 * 1000;
	}
	if (speed == 4) {
		loop_delay.tv_nsec = 200 * 1000 * 1000;
	}
	if (speed == 5) {
		loop_delay.tv_nsec = 100 * 1000 * 1000;
	}

	pthread_mutex_init(&mymtx, NULL);
	pthread_cond_init(&variable, NULL);

	data_t_t data = {.end = false};

	pthread_t movement;
	if (level == 2)
	{
		paint_obsticle(100, 100, get_color(255, 0, 0));
		paint_obsticle(200, 200, get_color(0, 0, 255));
	}

	pthread_create(&movement, NULL, move_thread, &data);
	printf("created thread\n");


	display_clean(parlcd_mem_base);


	bool quit = false;
	body[0][0] = 0;
	body[0][1] = 0;
	body[0][0] = 20;
	body[0][1] = 0;

	spown_food();

	while (quit == false) {
		input = getchar();
		switch (input) {
		case 'w':
			if (direction == 2 || direction == 4) {
				direction = 1;
			}
			break;
		case 's':
			if (direction == 2 || direction == 4) {
				direction = 3;
			}
			break;
		case 'a':
			if (direction == 1 || direction == 3) {
				direction = 4;
			}
			break;
		case 'd':
			if (direction == 1 || direction == 3) {
				direction = 2;
			}
			break;
		case 'q':
			quit = true;
			data.end = true;
			pthread_cond_broadcast(&variable);
			break;
		default:
			break;
		}
		pthread_mutex_lock(&mymtx);
		quit = data.end;
		pthread_mutex_unlock(&mymtx);
	}

	draw_string(0, 100, "GAME OVER", 5, 'M', get_color(255, 0, 0));
	sleep(2);
	int point = length - 2;
	printf("score is %d\n", length - 2);
	return point;
}

void *move_thread(void* data) {
	data_t_t *data_carry = (data_t_t*)data;
//move body
	bool quit = false;
	while (quit == false) {

		delete_square(body[length - 1][0], body[length - 1][1], get_color(0, 0, 0));

		//extend check
		if (first != last) {
			if (body[length - 1][0] == food[first][0]	&& body[length - 1][1] == food[first][1]) {
				body[length][0] = food[first][0];
				body[length][1] = food[first][1];
				first += 1;
				length += 1;
			}
		}

		for (int i = length - 1; i > 0; i -= 1 ) {
			body[i][0] = body[i - 1][0];
			body[i][1] = body[i - 1][1];
		}
		//move head
		if (direction == 1) {
			body[0][0] = body[1][0];
			body[0][1] = body[1][1] - 20;
			if (body[0][1] < 0) {
				quit = true;
				pthread_mutex_lock(&mymtx);
				data_carry -> end = true;
				pthread_mutex_unlock(&mymtx);
				break;
			}
		}
		if (direction == 2) {
			body[0][0] = body[1][0] + 20;
			body[0][1] = body[1][1];
			if (body[0][0] > 480) {
				quit = true;
				pthread_mutex_lock(&mymtx);
				data_carry -> end = true;
				pthread_mutex_unlock(&mymtx);
				break;
			}
		}
		if (direction == 3) {
			body[0][0] = body[1][0];
			body[0][1] = body[1][1] + 20;
			if (body[0][1] > 320) {
				quit = true;
				pthread_mutex_lock(&mymtx);
				data_carry -> end = true;
				pthread_mutex_unlock(&mymtx);
				break;
			}
		}
		if (direction == 4) {
			body[0][0] = body[1][0] - 20;
			body[0][1] = body[1][1];
			if (body[0][0] < 0) {
				quit = true;
				pthread_mutex_lock(&mymtx);
				data_carry -> end = true;
				pthread_mutex_unlock(&mymtx);
				break;
			}
		}

		//crash check
		for (int i = 1; i < length; i++) {
			if (body[0][0] == body[i][0] && body[0][1] == body[i][1]) {
				pthread_mutex_lock(&mymtx);
				data_carry -> end = true;
				pthread_mutex_unlock(&mymtx);
			}
		}

		if ((body[0][0] > 100 && body[0][0] < 150) || (body[0][1] > 100 && body[0][1] < 150)
		        || (body[0][0] > 200 && body[0][0] < 250) || (body[0][1] > 200 && body[0][1] < 250))
		{
			pthread_mutex_lock(&mymtx);
			data_carry -> end = true;
			pthread_mutex_unlock(&mymtx);
		}
		for (int i = 0; i < length; i ++) {
			paint_square(body[i][0], body[i][1], get_color(255, 255, 255));
		}
		//food check
		if (body[0][0] == food_x && body[0][1] == food_y) {
			food[last][0] = food_x;
			food[last][1] = food_y;
			last += 1;
			delete_square(food_x, food_y, get_color(0, 0, 0));
			spown_food();
		}

		display_update(parlcd_mem_base);
		clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);

		pthread_mutex_lock(&mymtx);
		quit = data_carry -> end;
		pthread_mutex_unlock(&mymtx);

		printf("thread closed\n");
		
	}
	return NULL;
}


//print one square of food or part of snake
	void paint_square(int x, int y, unsigned short color) {
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 20; j++) {
				draw_pixel(x + i, y + j, color);
			}
		}
	}
	void paint_obsticle(int x, int y, unsigned short color) {
		for (int i = 0; i < 50; i++) {
			for (int j = 0; j < 50; j++) {
				draw_pixel(x + i, y + j, color);
			}
		}
	}
//delete last part of snake
	void delete_square(int x, int y, unsigned short color) {
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 20; j++) {
				draw_pixel(x + i, y + j, color);
			}
		}
	}

//spown food randomly
	void spown_food(void) {
		food_x = (rand() % (24 - 0 + 1)) * 20;;
		food_y = (rand() % (16 - 0 + 1)) * 20;;
		//uint32_t colour = 0xff0000;
		paint_square(food_x, food_y, get_color(255, 255, 0));
	}




