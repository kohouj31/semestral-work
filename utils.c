//Jan Kohout 2020

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

void my_assert(bool r, const char *fcname, int line, const char *fname)
{
	if(!r)
	{
		fprintf(stderr, "ERROR: my_assert fail %s line %d in %s\n",fcname, line, fname);
	}
}
//memory allocation comtrol
void *my_alloc(size_t size)
{
	void *ret = malloc(size);
	if(!ret)
	{
		fprintf(stderr, "Unable to allocate memmory\n");
		exit(101);
	}
	return ret;
}
//creates file with name 'name'
FILE *crete_file(char *name)
{
	FILE *output;
	output=fopen(name,"w");
	if(output == NULL)
	{
		fprintf(stderr,"Unable to open file!");
		exit(100);
	}
	return output;
}
