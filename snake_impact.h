//Jan Kohout 2020

#include <stdint.h>
#include <stdio.h>
// shared data structure
typedef struct {
	bool quit;
	unsigned short *fb;
} data_t;


//trying multi thread
pthread_mutex_t mtx;
pthread_cond_t cond;

struct menu_t
{
	//maximum of entries that wil fiit with 3X zoom
	char main_str[7][45];
	//number of options
	int num_of_entries;
	//position of arrow
	int arrow_pos;
	//direction of arrow
	int def_pos;
};
typedef struct
{
	//store names of top 5
	char name[5][30];
	//store scores of the top 5
	char values[5][10];
	int saved;

} high_t;

//trying multi thread
pthread_mutex_t mtx;
pthread_cond_t cond;

unsigned char *led_mem_base;
unsigned char *parlcd_mem_base;

typedef struct
{
	high_t *snake;
	high_t *space;
	char placaes[5][5];
} high_scores_t;

bool welkom_lights(unsigned char *mem_base);
bool menu_lights(unsigned char *mem_base);
bool draw_pixel(int x, int y, unsigned short color);
int char_width(int ch);
void draw_char(int x, int y, char ch, int size, uint16_t color);
void display_update(unsigned char *parlcd_mem_base);
void display_clean(unsigned char *parlcd_mem_base);
void draw_string(int x, int y, char *str, int size, char lining, uint16_t color);
void menu(struct menu_t *menu, char lining, int size);
uint16_t get_color(int r, int g, int b);
bool check_end(data_t *d);
void call_termios(int reset);
bool settings_menu(struct menu_t *arr);
bool high_menu(struct menu_t *arr);
bool space_impact_menu(struct menu_t *arr);
bool read_high_scores(void);
bool write_high_scores(void);
void blink(void);
void clean_frame(void);
void free_frame(void);//todo
bool my_snake_menu(struct menu_t *arr);
